from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


@login_required
def receipts_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    contex = {
        "receipts": receipt,
    }

    return render(request, "receipts/list.html", contex)


@login_required
def create_receipts(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    else:
        form = ReceiptForm()
    contex = {
        "form": form,
    }
    return render(request, "receipts/create.html", contex)


@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    contex = {
        "categories": category,
    }
    return render(request, "categories/list.html", contex)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    contex = {
        "accounts": account,
    }
    return render(request, "accounts/list.html", contex)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")

    else:
        form = CategoryForm()
    contex = {
        "form": form,
    }
    return render(request, "categories/create.html", contex)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")

    else:
        form = AccountForm()
    contex = {
        "form": form,
    }
    return render(request, "accounts/create.html", contex)
