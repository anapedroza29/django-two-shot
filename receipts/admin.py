from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


@admin.register(ExpenseCategory)
class ExpenseCategoryForm(admin.ModelAdmin):
    pass


@admin.register(Account)
class AccountForm(admin.ModelAdmin):
    pass


@admin.register(Receipt)
class ReceiptForm(admin.ModelAdmin):
    pass
